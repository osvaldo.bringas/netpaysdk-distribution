Pod::Spec.new do |s|
    s.name         = "NetPaySDK"
    s.version      = "1.0.0"
    s.summary      = "A brief description of MyFramework project."
    s.description  = <<-DESC
    An extended description of MyFramework project.
    DESC
    s.homepage     = "https://docs.netpay.com.mx/docs"
    s.license = { :type => 'Copyright', :text => <<-LICENSE
                   Copyright 2021
                   Permission is granted to NetPay
                  LICENSE
                }
    s.author             = { "$(git config user.name)" => "$(git config user.email)" }
    s.source       = { :git => "https://gitlab.com/osvaldo.bringas/netpaysdk-distribution.git", :tag => "#{s.version}" }
    #https://gitlab.com/osvaldo.bringas/netpaysdk-distribution.git
    s.vendored_frameworks = "NetPaySDK.xcframework"
    s.platform = :ios
    s.swift_version = "5.4"
    s.ios.deployment_target  = '13.0'
end

Pod::Spec.new do |s|
    s.name         = "NetPaySDK"
    s.version      = "1.0.0"
    s.summary      = "Ecommerce SDK"
    s.description  = <<-DESC
    eCommerce SDK for NetPay integration
    DESC
    s.homepage     = "https://docs.netpay.com.mx/docs"
    s.license = { :type => 'Copyright', :text => <<-LICENSE
                   Copyright 2021
                   Permission is granted to Osvaldo Bringas
                  LICENSE
                }
    s.author             = { "Osvaldo Bringas" => "osvaldo.bringaz@gmail.com" }
    s.source       = { :git => "https://gitlab.com/osvaldo.bringas/netpaysdk-distribution.git", :tag => "#{s.version}" }
    s.vendored_frameworks = "NetPaySDK.xcframework"
    s.platform = :ios
    s.swift_version = "5.3"
    s.ios.deployment_target  = '13.0'
end
